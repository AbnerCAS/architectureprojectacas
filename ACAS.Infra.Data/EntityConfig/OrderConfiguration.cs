﻿using ACAS.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ACAS.Infra.Data.EntityConfig
{
    public class OrderConfiguration : EntityTypeConfiguration<Order>
    {
        public OrderConfiguration()
        {
            HasKey(p => p.ProductId);

            Property(p => p.Price)
                .IsRequired();

            //Inform to Entity that client has many orders
            HasRequired(p => p.Client)
                .WithMany()
                .HasForeignKey(p => p.ClientId);
        }
    }
}
