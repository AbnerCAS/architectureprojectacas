﻿using ACAS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACAS.Infra.Data.EntityConfig
{
    public class ClientConfiguration : EntityTypeConfiguration<Client>
    {
        //Constructor used to set the configuration for the client class 
        public ClientConfiguration()
        {
            //Set Id as Key P.S. It's not necessary to set this.
            HasKey(c => c.Id);

            //Set firstName as a required field
            Property(c => c.FirstName)
                .IsRequired();

            //Set Email as a required field
            Property(c => c.Email)
                .IsRequired();
        }
    }
}
