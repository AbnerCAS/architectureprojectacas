﻿using ACAS.Domain.Entities;
using ACAS.Infra.Data.EntityConfig;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;

namespace ACAS.Infra.Data.Context
{
    public class ProjectContext : DbContext
    {
        public ProjectContext()
            : base("ArcProjectConection")
        {

        }
        public DbSet<Client> Clients {get; set;}
        public DbSet<Order> Orders {get; set;}
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            //Remove all cascate delete option---------------------------------//
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            //-----------------------------------------------------------------//

            //Configure Entity to use varchar when prop is a string
            modelBuilder.Properties<string>()
                .Configure(p => p.HasColumnType("varchar").IsOptional());
            modelBuilder.Properties<DateTime>()
                .Configure(p => p.HasColumnType("datetime2"));

            //Configure EntityFramework to always set lenght to 100 if the prop is a string
            modelBuilder.Properties<string>()
                .Configure(p => p.HasMaxLength(100));

            //Add client configuration to the context
            modelBuilder.Configurations.Add(new ClientConfiguration());
            modelBuilder.Configurations.Add(new OrderConfiguration());
        }
        //TODO: Verify if this modification stay.
        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("CreatedAt") != null))
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Property("CreatedAt").CurrentValue = DateTime.Now;
                    entry.Property("Active").CurrentValue = true;
                }
                if (entry.State == EntityState.Modified)
                {
                    entry.Property("CreatedAt").IsModified = false;
                }
            }
            return base.SaveChanges();
        }
    }

    
}
