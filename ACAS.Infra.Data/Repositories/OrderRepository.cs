﻿using ACAS.Domain.Entities;
using ACAS.Domain.Interfaces;
using ACAS.Domain.Interfaces.Repositories;

namespace ACAS.Infra.Data.Repositories
{
    public class OrderRepository : RepositoryBase<Order>, IOrderRepository
    {
    }
}
