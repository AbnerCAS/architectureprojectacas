﻿
using System;
using System.Collections.Generic;

namespace ACAS.Domain.Entities
{

    /*******************
    * Type: Class
    * Name; Client
    * Function: It's a declation of the client object. It contains validation methods and 
    *   all the client props.
    *******************/
    public class Client
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Email { get; set; }
        public string Document { get; set; }
        public DateTime CreatedAt { get; set; }
        public virtual IEnumerable<Order> Orders { get; set; }

        public bool Active { get; set; }
    }
}
