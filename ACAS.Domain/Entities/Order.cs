﻿
using System;

namespace ACAS.Domain.Entities
{
    public class Order
    {
        public int ProductId { get; set; }
        public decimal Price { get; set; }
        public string ProductType { get; set; }
        public string ProductName { get; set; }
        public DateTime CreatedAt  { get; set; }
        public int ClientId { get; set; }
        public virtual Client Client { get; set; }
        public bool Active { get; set; }
    }
}
