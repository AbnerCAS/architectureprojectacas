﻿using System.Collections.Generic;


namespace ACAS.Domain.Interfaces.Repositories
{
    //*************************************************************//
    // Type: Interface
    // Function: This is a generic interface, responsible for expose the all the CRUD options.
    public interface IRepositoryBase<TEntity> where TEntity : class
    {
        void Add(TEntity obj);
        TEntity GetById(int id);
        IEnumerable<TEntity> GetAll();
        void Update(TEntity obj);
        void Remove(TEntity obj);
        void Dispose();
    }
}
