﻿

using ACAS.Domain.Entities;

namespace ACAS.Domain.Interfaces.Repositories
{
    public interface IClientRepository : IRepositoryBase<Client>
    {
    }
}
