﻿using ACAS.Domain.Entities;

namespace ACAS.Domain.Interfaces.Services
{
    public interface IClientService : IServiceBase<Client>
    {
    }
}
