﻿
using System;
using System.Collections.Generic;

namespace ACAS.Service.Services.ViewModel
{
    public class ClientModel
    {
        public int? Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Email { get; set; }
        public string Document { get; set; }
        public DateTime CreatedAt { get; set; }
        public virtual IEnumerable<OrderModel> Orders { get; set; }
        public bool Active { get; set; }
    }
}