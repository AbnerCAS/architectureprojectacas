﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACAS.Service.Services.ViewModel
{
    public class OrderModel
    {
        public int? ProductId { get; set; }
        public decimal Price { get; set; }
        public string ProductType { get; set; }
        public string ProductName { get; set; }
        public DateTime CreatedAt { get; set; }
        public int ClientId { get; set; }
        public bool Active { get; set; }
        public virtual ClientModel Client { get; set; }
    }
}