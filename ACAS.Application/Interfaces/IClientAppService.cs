﻿using ACAS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACAS.Service.Services.ViewModel;

namespace ACAS.Application.Interfaces
{
    public interface IClientAppService : IAppServiceBase<Client>
    {
        ClientModel AddClient(ClientModel client);
        ClientModel RemoveClientById(int id);
        ClientModel GetClientById(int id);
        IEnumerable<ClientModel> GetAllClients();
    }
}
