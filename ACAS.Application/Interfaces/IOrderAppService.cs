﻿using ACAS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACAS.Service.Services.ViewModel;

namespace ACAS.Application.Interfaces
{
    public interface IOrderAppService : IAppServiceBase<Order>
    {
        OrderModel AddNewOrder(OrderModel order);
        IEnumerable<OrderModel> GetAllOrders();
        IEnumerable<OrderModel> GetAllOrdersByClientId(int id);
        OrderModel GetOrderById(int id);
        OrderModel RemoveOrderbyId(int id);

    }
}
