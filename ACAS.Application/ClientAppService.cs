﻿using ACAS.Application.Interfaces;
using ACAS.Domain.Entities;
using ACAS.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACAS.Service.Services.ViewModel;
using AutoMapper;

namespace ACAS.Application
{
    public class ClientAppService : AppServiceBase<Client>, IClientAppService
    {
        private readonly IClientService _clientService;
        private readonly IOrderAppService _orderAppService;

        public ClientAppService(IClientService clientService, IOrderAppService orderAppService)
            : base(clientService)
        {
            _clientService = clientService;
            _orderAppService = orderAppService;
        }

        public ClientModel AddClient(ClientModel client)
        {
            try
            {
                if(string.IsNullOrWhiteSpace(client.FirstName))
                    throw new ArgumentException("The First Name of the client is required.");
                if(string.IsNullOrWhiteSpace(client.Email))
                    throw new ArgumentException("The Email of the client is required.");

                _clientService.Add(Mapper.Map<ClientModel, Client>(client));
                return client;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        public ClientModel RemoveClientById(int id)
        {
            try
            {
                var cli = _clientService.GetById(id);
                if(cli == null)
                    throw new ArgumentException("THe Client do not Exist.");

                var order = _orderAppService.GetAllOrdersByClientId(cli.Id).Any();

                if (order)
                    throw new ArgumentException("The Client cannot be delete because there is order on his/her name");

                _clientService.Remove(cli);
                return Mapper.Map<Client, ClientModel>(cli);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public ClientModel GetClientById(int id)
        {
            try
            {
                var cli = _clientService.GetById(id);
                if (cli == null)
                    throw new ArgumentException("THe Client do not Exist.");
                var orders= _orderAppService.GetAllOrdersByClientId(id);
                var Costumer = Mapper.Map<Client, ClientModel>(cli);
                Costumer.Orders = orders;

                return Costumer;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public IEnumerable<ClientModel> GetAllClients()
        {
            try
            {
                var cli = GetAll();
                return Mapper.Map<IEnumerable<Client>, IEnumerable<ClientModel>>(cli);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

    }
}
