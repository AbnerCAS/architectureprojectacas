﻿using ACAS.Domain.Entities;
using ACAS.Service.Services.ViewModel;
using AutoMapper;

namespace ACAS.Application.AutoMapper
{
    public class DomainToModelMappingProfile : Profile
    {
        public DomainToModelMappingProfile()
        {
            CreateMap<ClientModel, Client>();
            CreateMap<OrderModel, Order>();
        }
    }
}