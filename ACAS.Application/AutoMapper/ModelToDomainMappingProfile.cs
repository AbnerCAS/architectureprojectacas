﻿using ACAS.Domain.Entities;
using ACAS.Service.Services.ViewModel;
using AutoMapper;

namespace ACAS.Application.AutoMapper
{
    public class ModelToDomainMappingProfile : Profile
    {
        public ModelToDomainMappingProfile()
        {
            CreateMap<Client, ClientModel>();
            CreateMap<Order, OrderModel>();
        }
    }
}