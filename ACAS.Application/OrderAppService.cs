﻿using ACAS.Application.Interfaces;
using ACAS.Domain.Entities;
using ACAS.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACAS.Service.Services.ViewModel;
using AutoMapper;

namespace ACAS.Application
{
    public class OrderAppService : AppServiceBase<Order>, IOrderAppService
    {
        private readonly IOrderService _orderService;
        public OrderAppService(IOrderService orderService)
           : base(orderService)
        {
            _orderService = orderService;
        }

        public OrderModel AddNewOrder(OrderModel order)
        {
            try
            {
                if(order == null)
                    throw new ArgumentException("The order should not be null");
                if(string.IsNullOrEmpty(order.ProductName))
                    throw new ArgumentException("The Order Name is required");

                  _orderService.Add(Mapper.Map<OrderModel, Order>(order));
                 
                 return order;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        public IEnumerable<OrderModel> GetAllOrders()
        {
            try
            {
                return Mapper.Map<IEnumerable<Order>, IEnumerable<OrderModel>>(_orderService.GetAll());
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        public OrderModel GetOrderById(int id)
        {
            try
            {
                return Mapper.Map<Order, OrderModel>(_orderService.GetById(id));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        public IEnumerable<OrderModel> GetAllOrdersByClientId(int id)
        {
            try
            {
                var orders = _orderService.GetAll().Where(o => o.ClientId == id);

                return Mapper.Map<IEnumerable<Order>, IEnumerable<OrderModel>>(orders) ;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        public OrderModel RemoveOrderbyId(int id)
        {
            try
            {
                var order = _orderService.GetById(id);

                if(order== null)
                    throw  new ArgumentException("The Order do not Exist");
                _orderService.Remove(order);

                return Mapper.Map<Order, OrderModel>(order);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }


    }
}
