﻿using System.Collections.Generic;
using System.Web.Http;
using ACAS.Application;
using ACAS.Service.Services;
using ACAS.Application.Interfaces;
using ACAS.Service.Services.ViewModel;

namespace ACAS.API.Controllers
{
   
    [RoutePrefix("api/Client")]
    public class ClientController : ApiController
    {
        private readonly IClientAppService _clientAppService;
        
        public ClientController(IClientAppService clientAppService)
        {
            this._clientAppService = clientAppService;
        }

        public ClientController()
        {
                
        }

        [HttpGet]
        [Route("GetAll")]
        public IEnumerable<ClientModel> GetAllClient()
        {
            return _clientAppService.GetAllClients();
        }

        [HttpGet]
        [Route("{id}")]
        public ClientModel GetClientById(int id)
        {
            return _clientAppService.GetClientById(id);
        }

        [HttpPost]
        [Route("Create")]
        public string CreateClient([FromBody]ClientModel client)
        {
            _clientAppService.AddClient(client);
            return "Ok";
        }
        [HttpDelete]
        [Route("{id}/Delete")]
        public bool DeleteClient(int id)
        {
            return _clientAppService.RemoveClientById(id) == null;
        }

    }
}