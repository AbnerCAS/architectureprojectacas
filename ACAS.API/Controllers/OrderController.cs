﻿using ACAS.Service.Services.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using ACAS.Application.Interfaces;

namespace ACAS.API.Controllers
{
    [RoutePrefix("api/Order")]
    public class OrderController : ApiController
    {
        private readonly IOrderAppService _orderAppService;

        public OrderController(IOrderAppService orderAppService)
        {
            _orderAppService = orderAppService;
        }

        public OrderController()
        {
                
        }
        [HttpGet]
        [Route("GetAll")]
        public IEnumerable<OrderModel> GetAllOrders()
        {
            return _orderAppService.GetAllOrders();
        }

        [HttpGet]
        [Route("ClientId/{id}")]
        public IEnumerable<OrderModel> GetOrdersByClientId(int id)
        {
            return _orderAppService.GetAllOrdersByClientId(id);
        }
        [HttpGet]
        [Route("{id}")]
        public OrderModel GetOrdersById(int id)
        {
            return _orderAppService.GetOrderById(id);
        }

        [HttpPost]
        [Route("Create")]
        public string CreateOrder(OrderModel order)
        {
            _orderAppService.AddNewOrder(order);
            return "Ok";
        }
        [HttpDelete]
        [Route("{id}/Delete")]
        public bool DeleteClient(int id)
        {
            _orderAppService.RemoveOrderbyId(id);

            return true;
        }
    }
}